import toml
from os.path import expanduser

defaultConfig = {
    "directoriesToCheck": [
        expanduser("~"),
        expanduser("~/Desktop"),
        expanduser("~/Downloads")
    ],
    "fileDestinations": {
        expanduser("~/Documents"): [
            ".pdf",
            ".docx"
        ],
        expanduser("~/Downloads"): [
            ".zip",
            ".tar.gz",
            ".deb"
        ],
        expanduser("~/Music"): [
            ".mp3"
        ],
        expanduser("~/Pictures"): [
            ".png",
            ".webp",
            ".jpg",
            ".jpeg",
            ".svg"
        ],
        expanduser("~/Videos"): [
            ".mp4",
            ".flv",
            ".webm"
        ]
    }
}

with open("example_config.toml", "w") as configFile:
    toml.dump(defaultConfig, configFile)
