cleanithon (WIP)
======

A simple directory cleaner made in python.

Usage
-----
**(TODO)**

License
------
This project is licensed under the [GNU General Public License v3.0](LICENSE). The logo for this project (`icon.png`)[./assets/icon.png] is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
