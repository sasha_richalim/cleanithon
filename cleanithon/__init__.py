"""Top level package for cleanithon."""

__app_name__ =  "cleanithon"
__version__ = "1.0.0"

(
    SUCCESS,
    DIR_ERROR,
    FILE_ERROR,
) = range(3)


ERRORS = {
    DIR_ERROR: "config directory error",
    FILE_ERROR: "config file error",
}
