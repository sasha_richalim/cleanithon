from os.path import expanduser
import os
import shutil
from rich.console import Console
from rich import print as richprint
from time import sleep


def main():
    home = expanduser("~")
    dirsforcheck = [home, os.path.join(home, "Desktop"), os.path.join(home, "Downloads")]
    fidedestinations = {
        "Documents": [
            ".pdf",
            ".docx",
        ],
        "Downloads": [
            ".zip",
            ".tar.gz",
            ".deb",
        ],
        "Music": [
            ".mp3",
        ],
        "Pictures": [
            ".png",
            ".webp",
            ".jpg",
            ".jpeg",
            ".svg",
        ],
        "Videos": [
            ".mp4",
            ".flv",
            ".webm",
        ]
    }

    console = Console()

    counter_diff = 0

    for directory in dirsforcheck:
        for listdirItem in os.listdir(directory):
            for fileDestination, suffixes in fidedestinations.items():
                for suffix in suffixes:
                    if listdirItem.endswith(suffix):
                        if directory.endswith(fileDestination):
                            continue
                        sleep(1)
                        if os.path.exists(os.path.join(home, fileDestination, listdirItem)):
                            console.rule(
                                f'''File "{os.path.join(directory, listdirItem)}" already exists in "{os.path.join(
                                    home, fileDestination)}"''', align="center")
                            userinput = console.input("Do you want to overwrite it? (y/n): ")
                            if userinput != "y":
                                break
                        shutil.move(os.path.join(directory, listdirItem),
                                    os.path.join(home, fileDestination, listdirItem)
                                    )
                        counter_diff += 1
                        console.log(f'''Moving "{os.path.join(directory, listdirItem)}" to "{os.path.join(
                            home, fileDestination, listdirItem)}"''')

    if counter_diff == 0:
        richprint("[bold blue]Your directories are already clean!")


if __name__ == "__main__":
    main()
