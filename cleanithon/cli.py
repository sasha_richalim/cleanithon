"""This module provides the cleanithon CLI."""

from typing import Optional
from cleanithon import __app_name__, __version__, config, ERRORS
import typer

app = typer.Typer()


# Read the whole article and check how it matches with my config dir args
@app.command()
def init(
        config_path: str = typer.Option(
            None,
            "--config-path",
            "-cfg",
            prompt="Config file location:",
        )
) -> None:
    """Initialise config file"""
    app_init_error = config.init_app(config_path)
    if app_init_error:
        typer.secho(
            f'Creating config file failed with "{ERRORS[app_init_error]}"',
            fg=typer.colors.RED,
        )
        raise typer.Exit(1)

def _version_callback(value: bool) -> None:
    if value:
        typer.echo(f"{__app_name__} v{__version__}")
        raise typer.Exit()


@app.callback()
def main(
        version: Optional[bool] = typer.Option(
            None,
            "--version",
            "-v",
            help="Show the application's version and exit",
            callback=_version_callback,
            is_eager=True,
        )
) -> None:
    return
